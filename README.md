# firedb

**firedb** takes minilistic approac to integrating firestore to your **vuex** store with live updates from **firestore** and pushing changes you make locally to **firestore** automatically.

## Prerequisites
  - **firedb** depends on **firebase**. You should have already installed **firebase** in your repository.
  - 

## Installation
### Step 1: Download npm package
``` 
npm install firedb 
```
### Step 2: Import and register Vuex plugin
Import **firedb** in the file you are creating your store and pass **firedb** as one of the plugins.

```typescript
import firedb from "firedb";

const store = new Vuex.Store({
  state,
  mutations,
  plugins: [firedb] // register firedb as plugin
})
```

Thats it you are good to go!

## Registering and Unregistering firestore paths
**firedb** exposes `registerPath` and `unregisterPath` actions. As the name suggests, use `registerPath` to register a firestore path to a collection or document to start two way sync. Once you no longer need it simply call `unregisterPath`.

> ATTN: You need to make sure you have appropriate permissions to the path you are registering.

### Register
``` typescript
$store.dispatch("firedb/registerPath", "collection/doc");

// Now the document will be available in components like below

var doc = $store.state.firedb.collection.doc;
```

### Unregister
```typescript
$store.dispatch("firedb/unregisterPath", "collection/doc");

// $store.state.firedb.collection.doc is no longer avilable.
```

Things to note:
  - You can register both collections or documents paths.
  - **firedb** follows the same data structure as your firestore. E.g. a path `col1/doc1/col2` will be avilable in store as `state.firedb.col1.doc1.col2`.
  - You cannot register/unregister same path multiple times. **firedb** will ignore the duplicate reqeusts.
  - Unregister will also clean the state. Meaning if you had a path `collection/doc/` registered. After unregistering `$store.state.firedb.collection` will not exist.


## Saving changes / set
**firedb** exposes `set` action to update documents. It is recommended that any updates firestore documents are done through this action.
It is similar to Vue.set method.

```typescript
// You must have registered "path/to/collection" OR "path/to/collection/document" for this to work.
$store.dispatch("firedb/set", {path: "path/to/collection/document", value: {/*data*/}});

// If document doesnt exist in the collection, it will be created.
let doc = $store.state.firedb.path.to.collection.document;

// To remove a document, just pass value as null or undefined
$store.dispatch("firedb/set", {path: "path/to/collection/document", value: null});
$store.dispatch("firedb/set", {path: "path/to/collection/document"});

// Both of the above statement will remove document from collection
```

Things to note:
 - All updates will be sent to the server. If user do not have write access to the path referenced, this action will fail.
 - If document doesnt exist on referenced collection, it will be created.
 - You must have registered the path for this to work. For e.g. set to `a/b` refers to document `b` in collection `a`. You must have either registered path `a` or `a\b` before calling an update on `b`.
 - Any call to `set` on a collection path (odd length) will be log console error and ignored.
