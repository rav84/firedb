const unSubscribe = jest.fn();
let callback = null;

const onSnapshot = jest.fn((cb) => {
  callback = cb;
  cb({
    docChanges: () => {
      return [
        { type: "added", doc: { data: () => 2, id: "doc2" } },
        { type: "added", doc: { data: () => 1, id: "doc1" } },
        { type: "removed", doc: { id: "doc2" } }
      ];
    }
  });
  return unSubscribe;
});

const getUpdateSetAdd = function (ref) {
  return jest.fn((data) => {
    callback({
      docChanges: () => {
        return [{ type: "added", doc: { data: () => data, id: ref || "newref" } }]
      }
    });
  })
};

const getRemove = function (ref) {
  return jest.fn((data) => {
    callback({
      docChanges: () => {
        return [{ type: "removed", doc: { data: () => null, id: ref } }]
      }
    });
  })
};
const doc = jest.fn((ref) => {
  return {
    set: getUpdateSetAdd(ref),
    update: getUpdateSetAdd(ref),
    delete: getRemove(ref)
  };
});
const collection = jest.fn((path) => {
  return {
    where: collection,
    add: getUpdateSetAdd(null),
    doc,
    onSnapshot
  };
})

const firestore = jest.fn(() => { return { collection }; });

(firestore as any).FieldPath = { documentId: jest.fn().mockReturnValue(1) };

export default { firestore };