interface FireDBState {
    isBusy: boolean;
    _registeredPaths: Object;
    [i: string]: any;
}