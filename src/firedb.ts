import { Store, Module } from "vuex";
import log from "loglevel";
import mutations from "./mutations";
import actions from "./actions";

const fireDBModule: Module<FireDBState, any> = {};
fireDBModule.namespaced = true;
fireDBModule.state = { isBusy: false, _registeredPaths: {} };
fireDBModule.mutations = mutations;
fireDBModule.actions = actions;

export default function (store: Store<any>) {
  store.registerModule(["firedb"], fireDBModule);
  log.info("firedb module registered!");
}
