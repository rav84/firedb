import { ActionContext } from "vuex";
import log from "loglevel";
import firebase from "firebase/app";
import "firebase/firestore";
import { getNormalisedPathAndDocRef, extractQueries } from "./helpers";
import { _m } from "./mutations";
import _ from "lodash";

const subscribedPaths = {};

/**
 * Registers a path from firestore with live updates.
 * Use http url query pattern to add queries to your path
 * @param param0 ActionContext provided by Vuex
 * @param path Path to a collection or a document.
 */
async function registerPath({ state, commit }: ActionContext<FireDBState, any>, path: string) {
    try {
        commit(_m.SET_BUSY);
        const { pathWithoutQuery, queries } = extractQueries(path);
        const { collectionPath, docRef } = getNormalisedPathAndDocRef(pathWithoutQuery);
        path = collectionPath + (docRef ? ("/" + docRef) : ""); // Cleaned path
        if (state._registeredPaths[path]) { log.error("registerPath: Path already registered.", path); return; }
        commit(_m.ADD_REGISTERED_PATH, path);
        const db = firebase.firestore();
        let ref = docRef ?
            db.collection(collectionPath).where(firebase.firestore.FieldPath.documentId(), "==", docRef) :
            db.collection(collectionPath);
        queries.forEach((q) => {
            ref = ref.where(q[0], q[1], q[2]);
        });
        subscribedPaths[path] = ref.onSnapshot((snapshot) => {
            ////////////// Callback //////////////
            try {
                commit(_m.SET_BUSY);
                snapshot.docChanges().forEach((change) => {
                    // Possible change.type values: 'added' | 'removed' | 'modified'
                    const value = change.type === "removed" ? null : change.doc.data();
                    commit(_m.SET_DOC, { documentPath: collectionPath + "/" + change.doc.id, value });
                });
            } catch (e) { log.error(e); }
            finally { commit(_m.SET_BUSY, false); }
            ////////////// Callback //////////////
        });
    } finally { commit("setBusy", false); }
}

/**
 * Un-Registers a path from firestore and remove any data from state.
 * Un-Registers all registrations if no path is provided
 * @param param0 ActionContext provided by Vuex
 * @param path Path to a collection or a document, which was used when registering.
 */
async function unregisterPath({ state, commit }: ActionContext<FireDBState, any>, path: string) {
    try {
        commit(_m.SET_BUSY);
        const { collectionPath, docRef } = getNormalisedPathAndDocRef(path);
        path = collectionPath + (docRef ? ("/" + docRef) : ""); // Cleaned path
        if (!state._registeredPaths[path]) { log.error("unregisterPath: Path is not registered.", path); return; }
        subscribedPaths[path]();
        delete subscribedPaths[path];
        commit(_m.REMOVE_REGISTERED_PATH, path);
    } finally { commit(_m.SET_BUSY, false); }
}

/**
 * Sets a document on registered path.
 * Below list explains the operation executed based on payload
 * (collection path, value) => document added to collection
 * (document path, value) => document updated
 * (document path, no value) => remove document
 * other => ignored
 * @param param0 ActionContext provided by vuex
 * @param payload { path: path to a document for update/delete else to collection, value: value to set or null to remove document }
 */
async function set({ state, commit }: ActionContext<FireDBState, any>, payload: { path: string, value: any }) {
    try {
        commit(_m.SET_BUSY);
        const { collectionPath, docRef } = getNormalisedPathAndDocRef(payload.path);
        if (_.has(state._registeredPaths, payload.path) || _.has(state._registeredPaths, collectionPath)) {
            payload.value = JSON.parse(JSON.stringify(payload.value));
            const cRef = firebase.firestore().collection(collectionPath);
            const existingValue = _.get(state, `firedb.${payload.path.replace("/", ".")}`, null);
            // If it already exists
            if (docRef && payload.value && existingValue) {
                await cRef.doc(docRef).update(payload.value);
            }
            else if (docRef && payload.value && !existingValue) {
                await cRef.doc(docRef).set(payload.value);
            }
            else if (!docRef && payload.value) { await cRef.add(payload.value); }
            else if (docRef && !payload.value) { await cRef.doc(docRef).delete(); }
        } else {
            log.warn("firedb: Cannot set a path which is not registered.", payload);
        }
    } finally { commit(_m.SET_BUSY, false); }
}

export default { registerPath, unregisterPath, set };
export const _a = {
    REGISTER_PATH: "registerPath",
    UNREGISTER_PATH: "unregisterPath",
    SET: "set"
};