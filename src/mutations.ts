import Vue from "vue";
import log from "loglevel";
import { getPathRefs, getNormalisedPathAndDocRef } from "./helpers";

const busyStack: boolean[] = [];

/**
 * Sets a document in a collection.
 * @param state state, passed by vuex
 * @param payload { path: path to a doc, value: data object }
 */
function setDoc(state: FireDBState, payload: { documentPath: string, value?: any }) {
    const { collectionPath, docRef } = getNormalisedPathAndDocRef(payload.documentPath);
    if (!collectionPath || !docRef) { log.error("setDoc: Invalid payload.", payload); return; }
    for (const ref of getPathRefs(collectionPath)) {
        if (!state[ref]) { Vue.set(state, ref, {}); }
        state = state[ref];
    }
    if (payload.value) {
        Vue.set(state, docRef, payload.value);
    }
    else { Vue.delete(state, docRef); }
}

/**
 * Adds registered path to _registeredPaths
 * @param state state, passed by vuex
 * @param path path to register. collection/doc
 */
function addRegisteredPath(state: FireDBState, path: string) {
    if (!path) { log.error("addRegisteredPath: Invalid path.", path); return; }
    Vue.set(state._registeredPaths, path, true);
}

/**
 * Removes registered path from _registeredPaths and removes data from state
 * @param state state, passed by vuex
 * @param path path to unregister. collection/doc
 */
function removeRegisteredPath(state: FireDBState, path: string) {
    if (!state._registeredPaths[path]) { log.error("removeRegisteredPath: Path not registered.", path); return; }
    Vue.delete(state._registeredPaths, path);
    const { collectionPath, docRef } = getNormalisedPathAndDocRef(path);
    const refs = getPathRefs(collectionPath);
    const collectionName = refs.pop();
    // Get all paths which are deeper than path
    const commonRegisteredPaths = Object.keys(state._registeredPaths).filter(p => p.startsWith(path));
    for (const ref of refs) {
        if (!state[ref]) return;
        state = state[ref];
    }
    if (commonRegisteredPaths.length > 0) {
        // Another deepter path is already registered
        state = state[collectionName];
        if (docRef) state = state[docRef];
        const registeredProperties = commonRegisteredPaths.map(p => {
            return getPathRefs(p.replace(collectionName + (docRef ? "/" + docRef : ""), "")).shift()
        });
        Object.keys(state).forEach(prop => {
            if (!registeredProperties.find(p => p === prop)) Vue.delete(state, prop);
        });
    } else {
        Vue.delete(state, collectionName);
    }
}

/**
 * Sets firedb module isBusy flag. Can be used for loading-bars in UI.
 * @param state state, passed by vuex
 * @param value If not passed isBusy is set to true, else value.
 */
function setBusy(state: FireDBState, value?: boolean) {
    const v = typeof value === "undefined" ? true : value;
    if (v) {
        if (busyStack.length === 0) { state.isBusy = true; }
        busyStack.push(true);
    } else {
        busyStack.pop();
        if (busyStack.length === 0) { state.isBusy = false; }
    }
}

export default { setDoc, addRegisteredPath, removeRegisteredPath, setBusy };
export const _m = {
    SET_DOC: "setDoc",
    ADD_REGISTERED_PATH: "addRegisteredPath",
    REMOVE_REGISTERED_PATH: "removeRegisteredPath",
    SET_BUSY: "setBusy"
};